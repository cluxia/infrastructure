#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8 et ts=4 sts=4 sw=4
#
# Copyright © 2020 Maxime “pep” Buquet <pep@bouah.net>
#
# Distributed under terms of the GPL-3.0-or-later license.

"""
    Listens for queries and trigger repository updates.

    A goal is not to introduce any external dependencies to be able to version
    it alongside configuration management.

    It expects a POST query with headers `Content-Type` set to
    'application/json' and `Token` set to what's being configured when
    starting it. The query must contain a JSON dict with a single `id` field
    of type int.

    If the query is correct, the url will then be processed and the repository
    will be updated with the latest artifacts available.
"""

import os
import sys
import json
import shutil
import socket
import logging
import argparse
import subprocess
from pathlib import Path
from os.path import isfile, islink
from datetime import datetime
from functools import partial
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer


log = logging.getLogger('sync-arch-repo')

TOKEN = os.environ['REPO_TOKEN']
REPO_ARCHIVES_PATH = Path(os.environ['REPO_ARCHIVES_PATH'])
REPO_VHOST_PATH = Path(os.environ['REPO_VHOST_PATH'])

GITLAB_API_TOKEN = os.environ['GITLAB_API_TOKEN']
PROJECT_ID = 8523132
JOB_NAME = 'build-x86_64-cluxia'
ARCHIVE_URL = "https://gitlab.com/api/v4/projects/{}/jobs/artifacts/{}/download?job={}"


def update_repo(ref_name: str) -> bool:
    """Update Repository"""

    archive_path = REPO_ARCHIVES_PATH / 'archive.zip'
    repo_path = REPO_ARCHIVES_PATH / datetime.now().strftime('%Y%m%d%H%M%S')
    repo_path_tmp = REPO_ARCHIVES_PATH / 'tmp'

    try:
        log.debug('Create folder to extract the archive')
        os.makedirs(repo_path, mode=0o755, exist_ok=True)

        log.debug('Download the archive')
        # TODO: Return 400 instead of 500 if the url returns 404?
        url = ARCHIVE_URL.format(PROJECT_ID, ref_name, JOB_NAME)
        subprocess.run([
            'wget',
            '--header="PRIVATE-TOKEN: {}'.format(GITLAB_API_TOKEN),
            url,
            '-qO', str(archive_path),
        ], check=True)

        log.debug('Unzip it into the folder')
        shutil.unpack_archive(archive_path, repo_path)

        log.debug('Symlink atomically to the folder being served')
        os.symlink(repo_path, repo_path_tmp)
        os.rename(repo_path_tmp, REPO_VHOST_PATH)

        log.debug('Cleanup older folders')
        for path in os.listdir(REPO_ARCHIVES_PATH):
            fullpath = REPO_ARCHIVES_PATH / path
            if fullpath in (repo_path, REPO_VHOST_PATH):
                continue

            log.debug('Removing %s', fullpath)
            if isfile(fullpath) or islink(fullpath):
                os.unlink(fullpath)
            else:
                shutil.rmtree(fullpath)
    except Exception as exn:
        try:
            log.debug('An error occured. Cleaning up:\n%r', exn)
            os.unlink(archive_path)
            shutil.rmtree(repo_path)
            os.unlink(repo_path_tmp)
        except:
            pass
        return False
    return True


class WebhookHandler(BaseHTTPRequestHandler):
    """Listens to queries and trigger appropriate actions"""

    server_version = "WebhookHandler"

    def __init__(self, *args, token=None, **kwargs):
        if token is None:
            raise Exception('Missing parameters to WebhookHandler')

        self.token = token

        super().__init__(*args, **kwargs)

    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.end_headers()
        self.wfile.write(b"It Works!\n")

    def do_POST(self):
        if self.headers.get("Token") != self.token and \
           self.headers.get("Content-Type") != "application/json":
            self.send_response(HTTPStatus.FORBIDDEN)
            self.end_headers()
            self.wfile.write(b"Wrong Headers\n")
            return

        length = int(self.headers.get('Content-Length', '0'))
        body = self.rfile.read(length)

        log.debug('Got a request with a valid token and body: %r', body)

        try:
            data = json.loads(body)
            ref_name = data['ref']
        except Exception as exn:
            log.debug('An error occured while processing request body:\n%r', exn)
            self.send_response(HTTPStatus.BAD_REQUEST)
            self.end_headers()
            return

        if not update_repo(ref_name):
            self.send_response(HTTPStatus.INTERNAL_SERVER_ERROR)
            self.end_headers()
            return

        log.debug('Successfully updated repository!')

        self.send_response(HTTPStatus.OK)
        self.end_headers()


def build_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--bind', '-b', metavar='ADDRESS',
                        help='Specify alternate bind address '
                             '[default: all interfaces]')
    parser.add_argument('--port', '-p', action='store', default=8000,
                        type=int, nargs='?',
                        help='Specify alternate port [default: 8000]')
    parser.add_argument('--quiet', '-q', action='store_const',
                        default=logging.DEBUG, dest='loglevel',
                        const=logging.ERROR, help='Set logging to error')


    return parser


def _get_best_family(*address):
    infos = socket.getaddrinfo(
        *address,
        type=socket.SOCK_STREAM,
        flags=socket.AI_PASSIVE,
    )
    family, _type, _proto, _canonname, sockaddr = next(iter(infos))
    return family, sockaddr


def run_server(bind, port, token):
    """Spawn the HTTP server"""

    _, addr = _get_best_family(bind, port)
    handler_class = partial(WebhookHandler, token=token)
    with HTTPServer(addr, handler_class) as httpd:
        host, port = httpd.socket.getsockname()[:2]
        url_host = f'[{host}]' if ':' in host else host
        print(
            f'Serving HTTP on {host} port {port} '
            f'(http://{url_host}:{port}/) ...'
        )
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print('\nKeyboard interrupt received, exiting.')
            sys.exit(0)


if __name__ == '__main__':
    parser = build_parser()
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel)

    run_server(args.bind, args.port, TOKEN)
